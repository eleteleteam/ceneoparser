# Ceneo Parser v1.00 2018
#
# Authors: 
# Adam Włodarczyk
# Patryk Rębilas
# Bartosz Suder

import sys
import xml.etree.cElementTree as ET
import os
import time
import datetime

#------------------------------------------------------------------------------------------------#
#							   ELEMENT TRANSFORMATION PROCEDURES								 #
#------------------------------------------------------------------------------------------------#

def TransformBody(sourceString):
    sourceString = sourceString.replace('\r\n',' ')   
    sourceString = sourceString.replace('\r',' ')   
    sourceString = sourceString.replace('\n',' ')   
    sourceString = sourceString.replace('<br/>', ' ')
    sourceString = sourceString.replace('<br>', ' ')

    sourceString = sourceString.replace('&#260;','Ą')
    sourceString = sourceString.replace('&#261;','ą')
    sourceString = sourceString.replace('&#280;','Ę')
    sourceString = sourceString.replace('&#281;','ę')
    sourceString = sourceString.replace('&#211;','Ó')
    sourceString = sourceString.replace('&#243;','ó')
    sourceString = sourceString.replace('&#262;','Ć')
    sourceString = sourceString.replace('&#263;','ć')
    sourceString = sourceString.replace('&#321;','Ł')
    sourceString = sourceString.replace('&#322;','ł')
    sourceString = sourceString.replace('&#323;','Ń')
    sourceString = sourceString.replace('&#324;','ń')
    sourceString = sourceString.replace('&#346;','Ś')
    sourceString = sourceString.replace('&#347;','ś')
    sourceString = sourceString.replace('&#377;','Ź')
    sourceString = sourceString.replace('&#378;','ź')
    sourceString = sourceString.replace('&#379;','Ż')
    sourceString = sourceString.replace('&#380;','ż')

    sourceString = sourceString.replace('&gt;','>')
    sourceString = sourceString.replace('&quot;','"')
    sourceString = sourceString.replace('&amp;','&')
    sourceString = sourceString.replace('&lt;','<')
    
    sourceString = sourceString.replace('&#33;','!')    
    sourceString = sourceString.replace('&#34;','"')    
    sourceString = sourceString.replace('&#35;','#')    
    sourceString = sourceString.replace('&#36;','$')    
    sourceString = sourceString.replace('&#37;','%')    
    sourceString = sourceString.replace('&#38;','&')    
    sourceString = sourceString.replace('&#39;',"'")    
    sourceString = sourceString.replace('&#40;','(')    
    sourceString = sourceString.replace('&#41;',')')
    sourceString = sourceString.replace('&#42;','*')    
    sourceString = sourceString.replace('&#43;','+')    
    sourceString = sourceString.replace('&#44;',',')    
    sourceString = sourceString.replace('&#45;','-')    
    sourceString = sourceString.replace('&#46;','.')    
    sourceString = sourceString.replace('&#47;','/')    
    sourceString = sourceString.replace('&#58;',':')    
    sourceString = sourceString.replace('&#59;',';')    
    sourceString = sourceString.replace('&#60;','<')    
    sourceString = sourceString.replace('&#61;','=')    
    sourceString = sourceString.replace('&#62;','>')    
    sourceString = sourceString.replace('&#63;','?')    

    sourceString = sourceString.replace('&#64;','@')    
    sourceString = sourceString.replace('&#91;','[')    
    sourceString = sourceString.replace('&#92;','\\')    
    sourceString = sourceString.replace('&#93;',']')    
    sourceString = sourceString.replace('&#94;','^')    
    sourceString = sourceString.replace('&#95;','_')    
    sourceString = sourceString.replace('&#96;','`')    
    sourceString = sourceString.replace('&#123;','{')   
    sourceString = sourceString.replace('&#124;','|')   
    sourceString = sourceString.replace('&#125;','}')   
    sourceString = sourceString.replace('&#126;','~')   
    
    return sourceString


def TransformCommentCount(commentCount):
    if(commentCount == 'skomentuj tę opinię'):
        return '0'

    return commentCount[ : commentCount.find(' ')] 

def TransformRecommendation(recommendation):
    if(recommendation.find('Polecam') != -1):
        recommendation = '1'
    else:
        recommendation = '0'

    return recommendation
    
def TransformReviewScore(reviewScore):
    return reviewScore.replace(',','.')   
    
def TransformDate(dateString):
    dateString = str(time.mktime(datetime.datetime.strptime(dateString, "%Y-%m-%d %H:%M:%S").timetuple()))
    dateString = dateString[ : dateString.find('.')]
    return dateString
    
#------------------------------------------------------------------------------------------------#
#							   			TRANSFORM PROCESS										 #
#------------------------------------------------------------------------------------------------#
	
def Transform(productID):
    numberOfTransformedReviews = 0
    tree = ET.parse(productID + '_comments_E.xml')
    product = tree.getroot()

    for review in product.findall('review'):
        numberOfTransformedReviews += 1
        review.find('body').text = TransformBody(review.find('body').text)
        review.find('pros').text = TransformBody(review.find('pros').text)
        review.find('cons').text = TransformBody(review.find('cons').text)
        review.find('commentCount').text = TransformCommentCount(review.find('commentCount').text)
        review.find('recommendation').text = TransformRecommendation(review.find('recommendation').text)
        review.find('reviewScore').text = TransformReviewScore(review.find('reviewScore').text)
        review.find('commentDate').text = TransformDate(review.find('commentDate').text)
        
        for comment in review.findall('comment'):
            comment.find('body').text = TransformBody(comment.find('body').text)
            comment.find('commentDate').text = TransformDate(comment.find('commentDate').text)
            
            for subComment in comment.findall('subComment'):
                subComment.find('body').text = TransformBody(subComment.find('body').text)
                subComment.find('commentDate').text = TransformDate(subComment.find('commentDate').text)

    tree.write(productID + '_comments_T.xml')
    os.remove(productID + '_comments_E.xml')
    return numberOfTransformedReviews
    

    
    
    
    
    
    
    
    
    