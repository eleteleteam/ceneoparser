# Ceneo Parser v1.00 2018
#
# Authors: 
# Adam Włodarczyk
# Patryk Rębilas
# Bartosz Suder

import requests
import sys
import xml.etree.cElementTree as ET

#------------------------------------------------------------------------------------------------#
#                                     XML WRITE PROCEDURES                                       #
#------------------------------------------------------------------------------------------------#

def AddXMLSubComment(XMLnode, reviewerName, commentDate, productId, reviewId, commentId, body):
    ET.SubElement(XMLnode, "reviewerName")  .text   = reviewerName
    ET.SubElement(XMLnode, "commentDate")   .text   = commentDate
    ET.SubElement(XMLnode, "productID")     .text   = productId
    ET.SubElement(XMLnode, "reviewID")      .text   = reviewId
    ET.SubElement(XMLnode, "commentID")     .text   = commentId
    ET.SubElement(XMLnode, "body")          .text   = body
    
def AddXMLReview(XMLnode, reviewerName, commentDate, productId, reviewId, recommendation, reviewScore, upVotes, downVotes, body, pros, cons, commentCount):
    ET.SubElement(XMLnode, "reviewerName")  .text   = reviewerName
    ET.SubElement(XMLnode, "commentDate")   .text   = commentDate
    ET.SubElement(XMLnode, "productID")     .text   = productId
    ET.SubElement(XMLnode, "reviewID")      .text   = reviewId
    ET.SubElement(XMLnode, "recommendation").text   = recommendation
    ET.SubElement(XMLnode, "reviewScore")   .text   = reviewScore
    ET.SubElement(XMLnode, "upVotes")       .text   = upVotes
    ET.SubElement(XMLnode, "downVotes")     .text   = downVotes
    ET.SubElement(XMLnode, "body")          .text   = body
    ET.SubElement(XMLnode, "pros")          .text   = pros
    ET.SubElement(XMLnode, "cons")          .text   = cons
    ET.SubElement(XMLnode, "commentCount")  .text   = commentCount
    
def AddXMLProductInfo(XMLnode, productID, brand, model, category, reviewCount):
    ET.SubElement(XMLnode, "productID")     .text   = productID
    ET.SubElement(XMLnode, "brand")         .text   = brand
    ET.SubElement(XMLnode, "model")         .text   = model
    ET.SubElement(XMLnode, "category")      .text   = category
    ET.SubElement(XMLnode, "reviewCount")   .text   = reviewCount


#------------------------------------------------------------------------------------------------#
#                                     HTML PARSE PROCEDURES                                      #
#------------------------------------------------------------------------------------------------#
    
def FindParam(sourceString, key, termination):

    if(sourceString.find(key) == -1):
        return ''

    parameterStart = sourceString.find(key) + len(key)
    parameterEnd   = sourceString[parameterStart : ].find(termination) + parameterStart   
    
    parameter = sourceString[parameterStart : parameterEnd]
    
    l=list(parameter);
    i = 0
    
    while i<len(l):
        o=ord(l[i]);
        if o>65535:
            l[i]="{"+str(o)+"ū}";
        i+=1;
    
    parameter = "".join(l)
    
    parameter = parameter.rstrip()
    parameter = parameter.lstrip()
    return parameter
   
   
def GetSubComment(html, output, prodId, commId):

    reviewerName = FindParam(html, '<strong>',                        '<')
    commentDate  = FindParam(html, 'time datetime="',                 '"')
    body         = FindParam(html, '<p class="product-review-body">', '</p>')
    productId    = FindParam(html, 'data-product-id="',               '"')
    reviewId     = FindParam(html, 'data-review-id="',                '"')
    commentId    = FindParam(html, 'data-review-comment-id="',        '"')

    AddXMLSubComment(output, reviewerName, commentDate, prodId, reviewId, commId, body)
   
    
def GetComment(html, output, prodId):

    reviewerName = FindParam(html, '<strong>',                        '<')
    commentDate  = FindParam(html, 'time datetime="',                 '"')
    body         = FindParam(html, '<p class="product-review-body">', '</p>')
    productId    = FindParam(html, 'data-product-id="',               '"')
    reviewId     = FindParam(html, 'data-review-id="',                '"')
    commentId    = FindParam(html, 'data-review-comment-id="',        '"')
    
    
    AddXMLSubComment(output, reviewerName, commentDate, prodId, reviewId, commentId, body)
    
    html = html[html.find('<ol class="product-review-replies">'):]
    
    subCommentCount = 0
    
    while(html.find('<li class="product-review-reply">') != -1):
    
        subCommentCount += 1
        subCommentStart = html.find('<li class="product-review-reply">')
        subCommentEnd   = html[subCommentStart + 1: ].find('<li class="product-review-reply">')
        
        subComment = html[subCommentStart : subCommentEnd]
        
        XMLsubCommentNode = ET.SubElement(output, "subComment")
        GetSubComment(subComment, XMLsubCommentNode, prodId, commentId)
        
        html = html[subCommentEnd : ]
        
    ET.SubElement(output, "subCommentCount").text = str(subCommentCount)
        
def GetReview(html, output, prodId):

    reviewerName = FindParam(html, '<div class="reviewer-name-line">',  '<')
    productId    = FindParam(html, 'data-product-id="',                 '"')
    reviewId     = FindParam(html, 'data-review-id="',                  '"')

    if(html.find('<em class="product-recommended">') != -1):
        recommendation = 'Polecam'
    else:
        recommendation = 'Nie polecam'
    
    upVotes     = FindParam(html, '<span id="votes-yes-' + reviewId +'">', '<')
    downVotes   = FindParam(html, '<span id="votes-no-' + reviewId +'">',  '<')
    reviewScore = FindParam(html, '<span class="review-score-count">',     '/5')
    commentDate = FindParam(html, '<time datetime="',                      '"')
    body        = FindParam(html, '<p class="product-review-body">',       '</p>')
    commentCount = FindParam(html, 'product-review-comments-' + reviewId + '">' , '<')

    pros = []
    prosList = html[html.find('<span class="pros">Zalety</span>') : ]
    prosList = prosList[prosList.find('<ul>') : prosList.find('</ul>')]

    while(prosList.count('<li>') > 0):
    
        pros.append(FindParam(prosList, '<li>', '</li>'))
        prosList = prosList[prosList.find('<li>') + 1:  ]

    pros = str(pros)
        
    cons = []   
    consList = html[html.find('<span class="cons">Wady</span>') : ]
    consList = consList[consList.find('<ul>') : consList.find('</ul>')]
    
    while(consList.count('<li>') > 0):
    
        cons.append(FindParam(consList, '<li>', '</li>'))
        consList = consList[consList.find('<li>') + 1:  ]
        
    cons = str(cons)
                
    AddXMLReview(output, reviewerName, commentDate, prodId, reviewId, recommendation, reviewScore, upVotes, downVotes, body, pros, cons, commentCount)
    
    

    html = html[html.find('<ol class="product-review-comments">') : ]
    
    while(html.find('<li class="product-review-comment js_product-review-hook">') != -1):
    
        commentStart = html.find('<li class="product-review-comment js_product-review-hook">')
        commentEnd  = html[commentStart+1:].find('<li class="product-review-comment js_product-review-hook">')
        
        comment = html[commentStart : commentEnd]
        XMLcommentNode = ET.SubElement(output, "comment")
        GetComment(comment, XMLcommentNode, prodId)
        
        html = html[commentEnd : ]
    
    
def GetProductData(html, output, prodId):

    productID    = FindParam(html, 'data-product-id="',                   '"')
    brand        = FindParam(html, '<meta property="og:brand" content="', '"')
    model        = FindParam(html, '<title>',                     '-')
    category     = FindParam(html[html.rfind('data-category-id='):], '<span itemprop="title">',               '<')
    reviewCount  = FindParam(html, 'Opinie i Recenzje (',                 ')')
        
    XMLproductInfoNode = ET.SubElement(output, "productInfo")
    AddXMLProductInfo(XMLproductInfoNode, prodId, brand, model, category, reviewCount)
            
    if(reviewCount == ''):
        return
    
    commentsAquired = 0
    
    while(1):
        commentsAquired += 1

        reviewStart = html.find('<li class="review-box js_product-review">')
        reviewEnd   = html.find('<div class="review-icons">')

        review = html[reviewStart : reviewEnd]
        XMLreviewNode = ET.SubElement(output, "review")
        GetReview(review, XMLreviewNode, prodId)
        
        html = html[reviewEnd + 1:]
        
        if(html.find('<li class="review-box js_product-review">') == -1):
        
            if(html.find('<li class="page-arrow arrow-next">') != -1):
                nextPageLink = FindParam(html, '<li class="page-arrow arrow-next"><a href="', '" >')
                req = requests.get('https://www.ceneo.pl//' + nextPageLink)
                html = str(req.text)
            else:
                return commentsAquired

def Extract(productID):
    numberOfReviews = 0
    req = requests.get('https://www.ceneo.pl//' + productID + '#tab=reviews')
    html = str(req.text)
    
    if(html.find('Strona, której szukasz, nie została znaleziona.') != -1):
        return -1
    
    if(html.find('Wystąpił niespodziewany błąd.') != -1):
        return -1
    
    if(html.find('Telefony') == -1 and html.find('Komputery') == -1 and html.find('Fotografia') == -1):
        return -1
    
    product = ET.Element("product")
    numberOfReviews = GetProductData(html, product, productID)
    
    tree = ET.ElementTree(product)
    tree.write(productID + "_comments_E.xml")
    return numberOfReviews
    

  
