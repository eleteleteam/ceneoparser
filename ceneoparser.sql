-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 03 Sty 2018, 23:19
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `ceneoparser`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `1baseproddata`
--

CREATE TABLE `1baseproddata` (
  `Timestamp` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Category` varchar(50) COLLATE utf16_polish_ci NOT NULL,
  `Brand` varchar(50) COLLATE utf16_polish_ci NOT NULL,
  `Model` varchar(50) COLLATE utf16_polish_ci NOT NULL,
  `ReviewAmount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `2review`
--

CREATE TABLE `2review` (
  `Timestamp` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `ReviewID` int(11) NOT NULL,
  `CommentDate` varchar(20) COLLATE utf16_polish_ci NOT NULL,
  `ReviwerName` varchar(30) COLLATE utf16_polish_ci NOT NULL,
  `Recomendation` int(11) NOT NULL,
  `CommentScore` varchar(10) COLLATE utf16_polish_ci NOT NULL,
  `YesVote` int(11) NOT NULL,
  `NoVote` int(11) NOT NULL,
  `Body` varchar(5000) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `Prons` varchar(1000) COLLATE utf16_polish_ci NOT NULL,
  `Cons` varchar(1000) COLLATE utf16_polish_ci NOT NULL,
  `CommentsCnt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comment`
--

CREATE TABLE `comment` (
  `Timestamp` int(11) NOT NULL,
  `ReviewID` int(11) NOT NULL,
  `CommentID` int(11) NOT NULL,
  `ComentDate` varchar(20) COLLATE utf16_polish_ci NOT NULL,
  `CommenterName` varchar(30) COLLATE utf16_polish_ci NOT NULL,
  `CommentBody` varchar(2000) COLLATE utf16_polish_ci NOT NULL,
  `SubCommentCnt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `subcomment`
--

CREATE TABLE `subcomment` (
  `Timestamp` int(11) NOT NULL,
  `SubCommentID` int(11) NOT NULL,
  `SubCommentDate` varchar(20) COLLATE utf16_polish_ci NOT NULL,
  `SubCommentAuthor` varchar(30) COLLATE utf16_polish_ci NOT NULL,
  `SubCommentBody` varchar(2000) COLLATE utf16_polish_ci NOT NULL,
  `CommentID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `1baseproddata`
--
ALTER TABLE `1baseproddata`
  ADD PRIMARY KEY (`Timestamp`);

--
-- Indexes for table `2review`
--
ALTER TABLE `2review`
  ADD KEY `Timestamp` (`Timestamp`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD KEY `Timestamp` (`Timestamp`);

--
-- Indexes for table `subcomment`
--
ALTER TABLE `subcomment`
  ADD KEY `Timestamp` (`Timestamp`);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `2review`
--
ALTER TABLE `2review`
  ADD CONSTRAINT `2review_ibfk_1` FOREIGN KEY (`Timestamp`) REFERENCES `1baseproddata` (`Timestamp`);

--
-- Ograniczenia dla tabeli `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`Timestamp`) REFERENCES `2review` (`Timestamp`);

--
-- Ograniczenia dla tabeli `subcomment`
--
ALTER TABLE `subcomment`
  ADD CONSTRAINT `subcomment_ibfk_1` FOREIGN KEY (`Timestamp`) REFERENCES `comment` (`Timestamp`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
