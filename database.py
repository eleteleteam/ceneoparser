# Ceneo Parser v1.00 2018
#
# Authors: 
# Adam Włodarczyk
# Patryk Rębilas
# Bartosz Suder

import MySQLdb
from extract import *
import xml.etree.cElementTree as ET
import time

#------------------------------------------------------------------------------------------------#
#                                    DATABASE CONFIGURATION                                      #
#------------------------------------------------------------------------------------------------#

dbHost = "127.0.0.1"
dbUser = "palcek"
dbPass = "placur"
dbName = "ceneoparser"

def LoadDbConfig():
    tree = ET.parse('config.xml')
    root = tree.getroot()
    dbHost = root.find('dbHost').text
    dbUser = root.find('dbUser').text
    dbPass = root.find('dbPass').text
    dbName = root.find('dbName').text
    
    return "Configuration loaded:\nDbName:\t" + dbName + "\nDbHost:\t" + dbHost + "\nDbUser:\t" + dbUser
    
#------------------------------------------------------------------------------------------------#
#                                         INSERT COMMANDS                                        #
#------------------------------------------------------------------------------------------------#
    
def DbInsertProduct(timestamp, productId, category, brand, model, reviewAmount):
    db = MySQLdb.connect(host=dbHost, user=dbUser, password=dbPass, db=dbName, charset='utf8', use_unicode=True)
    cursor = db.cursor()     
    
    cursor.execute("SET NAMES utf8mb4;")
    cursor.execute("SET CHARACTER SET utf8mb4;")
    cursor.execute("SET character_set_connection=utf8mb4;")
    
    cursor.execute("INSERT INTO ceneoparser.1baseproddata (Timestamp, ProductID ,Category ,Brand ,Model ,ReviewAmount) VALUES ('" + timestamp + "', '" + productId + "','" + category + "','" + brand + "','" + model + "','" + reviewAmount +"')")
    db.commit()
    db.close()
    
    
def DbInsertReview(timestamp, reviewerName, commentDate, productID, reviewID, recommendation, reviewScore, upVotes,downVotes, body, pros, cons, commentCount):
    
    db = MySQLdb.connect(host=dbHost, user=dbUser, password=dbPass, db=dbName, charset='utf8', use_unicode=True)
    cursor = db.cursor()     
    
    cursor.execute("SET NAMES utf8mb4;")
    cursor.execute("SET CHARACTER SET utf8mb4;")
    cursor.execute("SET character_set_connection=utf8mb4;")
    
    body = body.replace('"', '""')
    body = body.replace("'", "''")
    
    pros = pros.replace('"', '""')
    pros = pros.replace("'", "''")
    
    cons = cons.replace('"', '""')
    cons = cons.replace("'", "''")
    
    insertCommand = "INSERT INTO ceneoparser.2review"               
    insertData = "(Timestamp, ProductID ,ReviewID ,CommentDate ,ReviwerName ,Recomendation, CommentScore, YesVote, NoVote, Body, Prons, Cons, CommentsCnt)" 
    insertValues = "VALUES ('" + timestamp + "', '" + productID + "','" + reviewID + "','" + commentDate + "','" + reviewerName + "','" + recommendation +"','" + reviewScore +"','" + upVotes +"','" + downVotes +"','" + body +"','" + pros +"','" + cons +"','" + commentCount +"')"

    cursor.execute(insertCommand + insertData + insertValues)
    db.commit()
    db.close()
    
def DbInsertComment(timestamp, reviewerName, commentDate, productId, reviewId, commentId, body, subCommentCount):

    db = MySQLdb.connect(host=dbHost, user=dbUser, password=dbPass, db=dbName, charset='utf8', use_unicode=True)
    cursor = db.cursor()     
    
    cursor.execute("SET NAMES utf8mb4;")
    cursor.execute("SET CHARACTER SET utf8mb4;")
    cursor.execute("SET character_set_connection=utf8mb4;")
    
    body = body.replace('"', '""')
    body = body.replace("'", "''")
    
    insertCommand = "INSERT INTO ceneoparser.comment"               
    insertData = "(Timestamp ,ReviewID ,CommentID ,ComentDate ,CommenterName, CommentBody, SubCommentCnt)" 
    insertValues = "VALUES ('" + timestamp + "', '" + reviewId + "','" + commentId + "','" + commentDate + "','" + reviewerName + "','" + body + "','" + subCommentCount + "')"

    cursor.execute(insertCommand + insertData + insertValues)
    db.commit()
    db.close()
    
def DbInsertSubComment(timestamp, reviewerName, commentDate, productId, reviewId, commentId, body):

    db = MySQLdb.connect(host=dbHost, user=dbUser, password=dbPass, db=dbName, charset='utf8', use_unicode=True)
    cursor = db.cursor()     
    
    cursor.execute("SET NAMES utf8mb4;")
    cursor.execute("SET CHARACTER SET utf8mb4;")
    cursor.execute("SET character_set_connection=utf8mb4;")
    
    body = body.replace('"', '""')
    body = body.replace("'", "''")
    
    insertCommand = "INSERT INTO ceneoparser.subcomment"               
    insertData = "(Timestamp ,SubCommentID ,SubCommentDate ,SubCommentAuthor ,SubCommentBody, CommentID)" 
    insertValues = "VALUES ('" + timestamp + "', '" + commentId + "','" + commentDate + "','" + reviewerName + "','" + body + "','" + reviewId + "')"

    cursor.execute(insertCommand + insertData + insertValues)
    db.commit()
    db.close()
    
#------------------------------------------------------------------------------------------------#
#                                         CLEAR PROCESS                                          #
#------------------------------------------------------------------------------------------------#
    
def ClearDatabase():
    db = MySQLdb.connect(host=dbHost, user=dbUser, password=dbPass, db=dbName)
    cursor = db.cursor()    
    cursor.execute("DELETE FROM subcomment")
    cursor.execute("DELETE FROM comment")
    cursor.execute("DELETE FROM 2review")
    cursor.execute("DELETE FROM 1baseproddata")

    db.commit()
    db.close()
    
    
#------------------------------------------------------------------------------------------------#
#                                         LOAD PROCESS                                           #
#------------------------------------------------------------------------------------------------#
    
def Load(productID):
    reviewCount = 0
    tree = ET.parse(productID + '_comments_T.xml')
    product = tree.getroot()
    productInfo = product.find('productInfo')
    timestamp = str(time.time())
    timestamp = timestamp[ : timestamp.find(".")]
    DbInsertProduct(timestamp, productInfo.find('productID').text, productInfo.find('category').text, productInfo.find('brand').text, productInfo.find('model').text, productInfo.find('reviewCount').text)
    
    for review in product.findall('review'):
        reviewCount += 1
        DbInsertReview( timestamp, 
                        review.find('reviewerName').text, 
                        review.find('commentDate').text, 
                        review.find('productID').text, 
                        review.find('reviewID').text, 
                        review.find('recommendation').text, 
                        review.find('reviewScore').text, 
                        review.find('upVotes').text, 
                        review.find('downVotes').text, 
                        review.find('body').text, 
                        review.find('pros').text, 
                        review.find('cons').text, 
                        review.find('commentCount').text)
        for comment in review.findall('comment'):
            DbInsertComment(timestamp, 
                            comment.find('reviewerName').text, 
                            comment.find('commentDate').text, 
                            comment.find('productID').text,
                            comment.find('reviewID').text,
                            comment.find('commentID').text,
                            comment.find('body').text,
                            comment.find('subCommentCount').text,)
            for subComment in comment.findall('subComment'):
                DbInsertSubComment(timestamp, 
                                subComment.find('reviewerName').text, 
                                subComment.find('commentDate').text, 
                                subComment.find('productID').text,
                                subComment.find('reviewID').text,
                                subComment.find('commentID').text,
                                subComment.find('body').text,)


    return reviewCount
    
    
#------------------------------------------------------------------------------------------------#
#                                         READ PROCESS                                           #
#------------------------------------------------------------------------------------------------#
    
def DbGetProduct(productId):
    db = MySQLdb.connect(host=dbHost, user=dbUser, password=dbPass, db=dbName, charset='utf8', use_unicode=True)
    cursor = db.cursor()     
    
    reviewCount = 0;
    
    timestamp = 0
    
    output = ET.Element("product")
    
    cursor.execute("SET NAMES utf8mb4;")
    cursor.execute("SET CHARACTER SET utf8mb4;")
    cursor.execute("SET character_set_connection=utf8mb4;")
    
    cursor.execute("SELECT * FROM 1baseproddata WHERE ProductID = " + str(productId) + " ORDER BY Timestamp desc limit 1;")
    cursor.fetchall()
        
    if(cursor.rowcount == 0):
        return -1
        
    for row in cursor:
        timestamp = str(row[0])
        XMLproductInfoNode = ET.SubElement(output, "productInfo")
        AddXMLProductInfo(XMLproductInfoNode, str(row[1]), str(row[3]), str(row[4]), str(row[2]), str(row[5]))
    
    cursor.execute("SELECT * FROM 2review WHERE ProductID = " + str(productId) + " AND Timestamp = " + str(timestamp) + ";")
    
    reviewData = cursor.fetchall()
    
    for review in reviewData:
        reviewCount += 1
    
        XMLreviewNode = ET.SubElement(output, "review")
        
        body = str(review[9]).replace('""', '"')
        body = body.replace("''", "'")
        
        pros = str(review[10]).replace('""', '"')
        pros = pros.replace("''", "'")
        
        cons = str(review[11]).replace('""', '"')
        cons = cons.replace("''", "'")
        
        AddXMLReview(XMLreviewNode, str(review[4]), str(review[3]), str(review[1]), str(review[2]), str(review[5]), str(review[6]), str(review[7]), str(review[8]), body, pros, cons, str(review[12])) 
        reviewId = str(review[2])
        
        cursor.execute("SELECT * FROM comment WHERE ReviewID = " + reviewId + " AND Timestamp = " + timestamp + ";")
        commentData = cursor.fetchall()
        for comment in commentData:
            XMLcommentNode = ET.SubElement(XMLreviewNode, "comment")
            
            body = str(comment[5]).replace('""', '"')
            body = body.replace("''", "'")
            AddXMLSubComment(XMLcommentNode, str(comment[4]), str(comment[3]), productId, str(comment[1]), str(comment[2]), body)
            
            commentId = str(comment[2])
            cursor.execute("SELECT * FROM subcomment WHERE SubCommentID = " + commentId + " AND Timestamp = " + timestamp + ";")
            subCommentData = cursor.fetchall()
            for subcomment in subCommentData:
                XMLsubcommentNode = ET.SubElement(XMLcommentNode, "subComment")
                
                body = str(subcomment[4]).replace('""', '"')
                body = body.replace("''", "'")
                
                AddXMLSubComment(XMLsubcommentNode, str(subcomment[3]), str(subcomment[2]), productId, str(subcomment[5]), str(subcomment[1]), body)

            ET.SubElement(XMLcommentNode, "subCommentCount").text = str(comment[6])
            
    tree = ET.ElementTree(output)
    tree.write(productId + "_comments_R.xml")
    db.commit()
    db.close()
    
    return reviewCount