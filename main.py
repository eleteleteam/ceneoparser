# Ceneo Parser v1.00 2018
#
# Authors: 
# Adam Włodarczyk
# Patryk Rębilas
# Bartosz Suder

import requests
import sys
import xml.etree.cElementTree as ET
import os

from extract import Extract
from transform import Transform
from datetime import datetime
from database import *
from export import *

from tkinter import *
from tkinter import ttk
import tkinter as tk
import tkinter.scrolledtext as tkst

welcomeText = 'Welcome to CeneoParser v1.00.\nAuthors:\nAdam Włodarczyk\nBartosz Suder\nPatryk Rębilas'

#------------------------------------------------------------------------------------------------#
#                                       DISPLAY PROCEDURES                                       #
#------------------------------------------------------------------------------------------------#

def Log(txt):
    logWindow.config(state="normal")
    logWindow.insert(tk.INSERT, '--------- ' + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' ---------\n' + txt + "\n\n")
    logWindow.config(state="disabled")
    logWindow.see("end")
    
def ClearScreen():
    outputWindow.config(state="normal")
    outputWindow.delete("1.0", "end")
    outputWindow.config(state="disabled")
    
def Display(txt):
    outputWindow.config(state="normal")
    outputWindow.insert(tk.INSERT, txt)
    outputWindow.config(state="disabled")               
    
def DisplayFile(fileName):
    tree = ET.parse(fileName)
    product = tree.getroot()
    productInfo = product.find('productInfo')
    
    ClearScreen()

    Display('File: '        + fileName                           + '\n\n')
    
    Display('Product ID: '  + productInfo.find('productID').text     + '\n')
    Display('Brand: '       + productInfo.find('brand').text         + '\n')
    Display('Model: '       + productInfo.find('model').text         + '\n')
    Display('Category: '    + productInfo.find('category').text      + '\n')
    Display('Reviews: '     + productInfo.find('reviewCount').text   + '\n\n')
    
    for review in product.findall('review'):
        Display('REVIEW ------------------------------------------------\n')
        Display('Author: '          + review.find('reviewerName').text      + '\n')
        Display('Date: '            + review.find('commentDate').text       + '\n')
        Display('Product ID: '      + review.find('productID').text         + '\n')
        Display('Review ID: '       + review.find('reviewID').text          + '\n')
        Display('Recommendation: '  + review.find('recommendation').text    + '\n')
        Display('Review score: '    + review.find('reviewScore').text       + '\n')
        Display('UpVotes: '         + review.find('upVotes').text           + '\n')
        Display('DownVotes: '       + review.find('downVotes').text         + '\n')
        Display('Body: '            + review.find('body').text              + '\n')
        Display('Pros: '            + review.find('pros').text              + '\n')
        Display('Cons: '            + review.find('cons').text              + '\n')
        Display('Comments: '        + review.find('commentCount').text      + '\n\n')
            
        for comment in review.findall('comment'):
            Display('COMMENT ------------------------------------------------\n')
            Display('Author: '          + comment.find('reviewerName').text      + '\n')
            Display('Date: '            + comment.find('commentDate').text       + '\n')
            Display('Product ID: '      + comment.find('productID').text         + '\n')
            Display('Review ID: '       + comment.find('reviewID').text          + '\n')
            Display('Comment ID: '      + comment.find('commentID').text         + '\n')
            Display('Body: '            + comment.find('body').text              + '\n')
            Display('Sub comments: '    + comment.find('subCommentCount').text   + '\n\n')

            for subComment in comment.findall('subComment'):
                Display('SUB_COMMENT ------------------------------------------------\n')
                Display('Author: '          + subComment.find('reviewerName').text      + '\n')
                Display('Date: '            + subComment.find('commentDate').text       + '\n')
                Display('Product ID: '      + subComment.find('productID').text         + '\n')
                Display('Review ID: '       + subComment.find('reviewID').text          + '\n')
                Display('Comment ID: '      + subComment.find('commentID').text         + '\n')
                Display('Body: '            + subComment.find('body').text              + '\n\n')


                
#------------------------------------------------------------------------------------------------#
#                                       BUTTON PROCEDURES                                        #
#------------------------------------------------------------------------------------------------#
    
def Button_Extract(*args):
    reviewCount = Extract(productID.get())
    
    if(reviewCount == -1):
        Log('EXTRACT ERROR:\nInvalid product ID.')
        return 0
        
    Log('EXTRACT process finished:\nProduct ID: ' + productID.get() + '\n' + str(reviewCount) +' reviews extracted.')
    DisplayFile(productID.get() + '_comments_E.XML')
    processedProductID.set(productID.get())
    buttonE.config(state = "disabled")
    buttonT.config(state = "enabled")
        
def Button_Transform(*args):
    reviewCount = Transform(processedProductID.get())
    DisplayFile(productID.get() + '_comments_T.XML')
    Log('TRANSFORM process finished:\nProduct ID: ' + productID.get() + '\n' + str(reviewCount) +' reviews transformed.')
    buttonT.config(state = "disabled")
    buttonL.config(state = "enabled")
    
    if os.path.exists(productID.get() + '_comments_E.XML'):
        os.remove(productID.get() + '_comments_E.XML')
    
def Button_Load(*args):
    reviewCount = Load(processedProductID.get())
    ClearScreen()
    Log('LOAD process finished:\nProduct ID: ' + productID.get() + '\n' + str(reviewCount) +' reviews loaded.')
    buttonL.config(state = "disabled")
    buttonE.config(state = "enabled")
    
    if os.path.exists(productID.get() + '_comments_T.XML'):
        os.remove(productID.get() + '_comments_T.XML')
    
def Button_ETL(*args):
    ClearScreen()

    reviewCount = Extract(productID.get())
    if(reviewCount == -1):
        Log('EXTRACT ERROR:\nInvalid product ID.')
        return 0
    Log('EXTRACT process finished:\nProduct ID: ' + productID.get() + '\n' + str(reviewCount) +' reviews extracted.')
    processedProductID.set(productID.get())

    reviewCount = Transform(processedProductID.get())
    Log('TRANSFORM process finished:\nProduct ID: ' + productID.get() + '\n' + str(reviewCount) +' reviews transformed.')

    if os.path.exists(productID.get() + '_comments_E.XML'):
        os.remove(productID.get() + '_comments_E.XML')
        
    reviewCount = Load(processedProductID.get())
    Log('LOAD process finished:\nProduct ID: ' + productID.get() + '\n' + str(reviewCount) +' reviews loaded.')

    if os.path.exists(productID.get() + '_comments_T.XML'):
        os.remove(productID.get() + '_comments_T.XML')
    
def Button_CLEAR(*args):
    ClearDatabase()
    Log('CLEAR process finished:\nDatabase cleared.')
    
def Button_READ(*args):
    reviewCount = DbGetProduct(productID.get())
    
    if(reviewCount == -1):
        Log('READ ERROR:\nNo such product ID in database.')
        return 0
    
    DisplayFile(productID.get() + '_comments_R.XML')
    Log('READ process finished:\nProduct ID: ' + productID.get() + '\n' + str(reviewCount) +' reviews acquired.\nFile: ' + productID.get() + '_comments_R.XML')
    
def Button_ExportCSV(*args):
    reviewCount = DbGetProduct(productID.get())
    
    if(reviewCount == -1):
        Log('READ ERROR:\nNo such product ID in database.')
        return 0
        
    ExportToCSV(productID.get())
    
    if os.path.exists(productID.get() + '_comments_R.XML'):
        os.remove(productID.get() + '_comments_R.XML')
    
    Log('READ process finished:\nProduct ID: ' + productID.get() + '\n' + str(reviewCount) +' reviews acquired.\nFile: ' + productID.get() + '_comments_CSV.csv')

#------------------------------------------------------------------------------------------------#
#                                      GRAPHIC USER INTERFACE                                    #
#------------------------------------------------------------------------------------------------#

root = Tk()
root.title("CeneoParser by Eletele-Team(R)")
root.resizable(width=False, height=False)
#                                 left, up, right, down
mainframe = ttk.Frame(root)
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)


productID = StringVar()
processedProductID = StringVar()

label = ttk.Label(mainframe, text="Enter product ID:")
label.grid(column=1, row=1, sticky=(W, E), padx=10, pady=(10,0), columnspan=3)

inputProductID = ttk.Entry(mainframe, width=30, textvariable=productID)
inputProductID.grid(column=1, row=2, sticky=(W, E), padx=10, pady=(0,10), columnspan=3)

buttonE = ttk.Button(mainframe, text="E", width=9, command=Button_Extract, state="enabled")
buttonE.grid(column=1, row=3, padx=(10,0), pady=(10,0))

buttonT = ttk.Button(mainframe, text="T", width=9, command=Button_Transform, state="disabled")
buttonT.grid(column=2, row=3, pady=(10,0))

buttonL = ttk.Button(mainframe, text="L", width=9, command=Button_Load, state="disabled")
buttonL.grid(column=3, row=3, padx=(0,10), pady=(10,0))

buttonETL = ttk.Button(mainframe, text="ETL", width= 30, command=Button_ETL, state="enabled")
buttonETL.grid(column=1, row=4, columnspan=3, padx=10, pady=(0,10))

buttonRead = ttk.Button(mainframe, text="READ [XML]", width= 20, command=Button_READ, state="enabled")
buttonRead.grid(column=1, row=5, columnspan=3, padx=10, pady=(10,0))

buttonExportCSV = ttk.Button(mainframe, text="READ [CSV]", width= 20, command=Button_ExportCSV, state="enabled")
buttonExportCSV.grid(column=1, row=6, columnspan=3, padx=10, pady=(0,10))

buttonClear = ttk.Button(mainframe, text="CLEAR DATABASE", width= 20, command=Button_CLEAR, state="enabled")
buttonClear.grid(column=1, row=7, columnspan=3, padx=10, pady=(10,10))

logWindow = tkst.ScrolledText(master = mainframe, wrap= tk.WORD, width= 30, height = 9,  state="disabled")
logWindow.grid(column=1, row=8, columnspan=3, padx=10, pady=10)
logWindow.config(font=("arial",8))

outputWindow = tkst.ScrolledText(master = mainframe, wrap= tk.WORD, state="disabled", width= 60, height=25)
outputWindow.grid(column=4, row=1, columnspan=1, rowspan=8, padx=10, pady=10)
outputWindow.config(font=("arial",8))

Log(welcomeText)
Log(LoadDbConfig())

root.mainloop()





