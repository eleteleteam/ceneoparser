# Ceneo Parser v1.00 2018
#
# Authors: 
# Adam Włodarczyk
# Patryk Rębilas
# Bartosz Suder

import sys
import xml.etree.cElementTree as ET
import os

#------------------------------------------------------------------------------------------------#
#									 		CSV EXPORT											 #
#------------------------------------------------------------------------------------------------#

def ExportToCSV(productId):
    file = open(productId + '_comments_CSV.csv', 'w', newline='', encoding="utf-16")
    tree = ET.parse(productId + '_comments_R.xml')
    product = tree.getroot()
    productInfo = product.find('productInfo')
    
    file.write('Product ID\t'  + productInfo.find('productID').text     + '\n')
    file.write('Brand\t'       + productInfo.find('brand').text         + '\n')
    file.write('Model\t'       + productInfo.find('model').text         + '\n')
    file.write('Category\t'    + productInfo.find('category').text      + '\n')
    file.write('Reviews\t'     + productInfo.find('reviewCount').text   + '\n\n')
    
    file.write('Author\tDate\tProduct ID\tReview ID\tRecommendation\tReview score\tUpVotes\tDownVotes\tBody\tPros\tCons\tComments\n')
    
    
    for review in product.findall('review'):
        file.write(review.find('reviewerName').text      + '\t')
        file.write(review.find('commentDate').text       + '\t')
        file.write(review.find('productID').text         + '\t')
        file.write(review.find('reviewID').text          + '\t')
        file.write(review.find('recommendation').text    + '\t')
        file.write(review.find('reviewScore').text       + '\t')
        file.write(review.find('upVotes').text           + '\t')
        file.write(review.find('downVotes').text         + '\t')
        file.write(review.find('body').text              + '\t')
        file.write(review.find('pros').text              + '\t')
        file.write(review.find('cons').text              + '\t')
        file.write(review.find('commentCount').text      + '\n')

    file.close()